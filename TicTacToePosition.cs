namespace TicTacToe;
public class TicTacToePosition {
    private TicTacToeMarks _Mark;

    public TicTacToePosition() {
        _Mark = TicTacToeMarks.Empty;
    }
    public TicTacToeMarks Mark {
        get { return _Mark;}
        set{
            if(value != TicTacToeMarks.X && value != TicTacToeMarks.O && value!= TicTacToeMarks.Empty) {
                throw new ArgumentException("This isn't a valid value");
            } else if(_Mark != TicTacToeMarks.Empty) {
                throw new ArgumentException("There is already a mark in here!");
            } else {
                _Mark = value;
            };
        }
    }

    public override string ToString() {
        if(_Mark == TicTacToeMarks.X) {
            return "X";
        }else if(_Mark == TicTacToeMarks.O) {
            return "O";
        } else {
            return " ";
        }
    }
}