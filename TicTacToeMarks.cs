namespace TicTacToe;

public enum TicTacToeMarks {
    X,
    O,
    Empty
}