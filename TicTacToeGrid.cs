﻿using System;
using System.Globalization;

namespace TicTacToe
{
    public class TicTacToeGrid
    {
        TicTacToePosition[,] grid { get; set; }
        string _Winner {get;}

        public TicTacToeGrid()
        {
            grid = new TicTacToePosition[3, 3];
            for (int i = 0; i < grid.GetLength(0); i++)
            {
                for (int j = 0; j < grid.GetLength(1); j++)
                {
                    grid[i, j] = new TicTacToePosition();
                }
            }
        }

        public TicTacToeMarks StringToMark(string input) {
            if(input.ToUpper().Equals("X")) {
                return TicTacToeMarks.X;
            }else if(input.ToUpper().Equals("O")) {
                return TicTacToeMarks.O;
            } else {
                return TicTacToeMarks.Empty;
            }
        }
        public void PlaceCharacter(TicTacToePosition symbol)
        {
            while (symbol.Mark != TicTacToeMarks.X && symbol.Mark != TicTacToeMarks.O)
            {
                Console.WriteLine("Please enter a valid character: ");
                symbol.Mark = StringToMark(Console.ReadLine());
            }

            Console.WriteLine("Please enter the row where you would like to place it");
            int row = Convert.ToInt32(Console.ReadLine()) - 1;
            while (row < 0 || row > 2)
            {
                Console.WriteLine("Please enter a valid row [1-3]");
                row = Convert.ToInt32(Console.ReadLine()) - 1;
            }
            Console.WriteLine("Please enter the column where you would like to place it");
            int column = Convert.ToInt32(Console.ReadLine()) - 1;
            while (column < 0 || column > 2)
            {
                Console.WriteLine("Please enter a valid column [1-3]");
                column = Convert.ToInt32(Console.ReadLine()) - 1;
            }

            grid[row, column].Mark = symbol.Mark;
        }

        public void PrintGrid()
        {
            String horizontalLine = "---------------";
            String verticalLine = " | ";
            String output = "";
            for (int i = 0; i < grid.GetLength(0); i++)
            {
                for (int j = 0; j < grid.GetLength(1); j++)
                {
                    output += " " + grid[i, j] + " ";
                    if (j < grid.GetLength(1) - 1)
                    {
                        output += verticalLine;
                    }
                }
                output += "\n";
                if (i < grid.GetLength(0) - 1)
                {
                    output += horizontalLine + "\n";
                }
            }
            Console.WriteLine(output);
            
        }
        
        public string CheckRows() 
        {

            for( int i = 0; i < grid.GetLength(0); i++)
            {
                TicTacToePosition firstSymbol = grid[i , 0]; //Check the symbol in the first column
                if(firstSymbol.Mark != TicTacToeMarks.Empty)
                {
                    bool nextSymbol = true; //Make it false when its not the same
                    for(int j = 1; j < grid.GetLength(1); j++) 
                    {
                        if(grid[i , j].ToString()  != firstSymbol.ToString() ) //if the next symbol is not equal to the first: make it false and break the loop
                        {
                            nextSymbol = false;
                            break;
                        }
                    }
                    if(nextSymbol)
                    {
                        return firstSymbol.ToString();
                    }
                }
            }
            return " ";
        }

        public string CheckColumns()
        {
            for(int i =0; i < grid.GetLength(1); i++)
            {
                TicTacToePosition firstSymbol = grid[0, i];
                if(firstSymbol.Mark != TicTacToeMarks.Empty)
                {
                    bool nextSymbol = true;
                    for(int j = 1;  j < grid.GetLength(0); j++)
                    {
                        if(grid[j, i].ToString() != firstSymbol.ToString())
                        {
                            nextSymbol = false;
                            break;
                        }
                    }
                    if(nextSymbol)
                    {
                        return firstSymbol.ToString();
                    }
                }
            }
            return " ";
        }

        public string CheckDiagonals()
        {
            TicTacToePosition firstSymbol = grid[0, 0]; //Top left to bot right
            if (firstSymbol.Mark != TicTacToeMarks.Empty)
            {
                bool nextSymbols  = true;
                for (int i = 1; i < grid.GetLength(0); i++)
                {
                    if (grid[i, i].ToString()  != firstSymbol.ToString() )
                    {
                        nextSymbols = false;
                        break;
                    }
                }
                if (nextSymbols)
                {
                    return firstSymbol.ToString();
                }
            }
            firstSymbol = grid[0, grid.GetLength(1) - 1]; //Top right to bot left
            if (firstSymbol.Mark != TicTacToeMarks.Empty)
            {
                bool nextSymbols = true;
                for (int i = 1; i < grid.GetLength(0); i++)
                {
                    if (grid[i, grid.GetLength(1) - 1 - i].ToString()  != firstSymbol.ToString() )
                    {
                        nextSymbols = false;
                        break;
                    }
                }
                if (nextSymbols)
                {
                    return firstSymbol.ToString();
                }
            }

            return " ";
        }
        public bool FilledGrid()
        {
            for (int i = 0; i < grid.GetLength(0); i++)
            {
                for (int j = 0; j < grid.GetLength(1); j++)
                {
                    if (grid[i, j].Mark == TicTacToeMarks.Empty)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        public string CheckGrid() {
            if(CheckRows() != " ") 
            {
                return CheckRows();
            }
            else if(CheckColumns() != " ")
            {
                return CheckColumns();
            }
            else if(CheckDiagonals() != " ")
            {
                return CheckDiagonals();
            }
            if(FilledGrid())
            {
                return "Tie";
            }
            return "";
        }

        public string Winner {
            get
            {
                string result = CheckGrid();
                if(result != "") 
                {
                    if(result == "Tie")
                    {
                        return "It's a Tie!!!!";
                    }
                    else
                    {
                        return $"Player {result} wins!!!!";
                    }
                } 
                else
                {
                    return "Ongoing";
                }
            }
        }
    }

}
