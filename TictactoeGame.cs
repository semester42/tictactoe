using System;

namespace TicTacToe
{
    public class TicTacToeGame
    {
        public static void Main(string[] args)
        {
            TicTacToeGrid grid = new TicTacToeGrid();
            TicTacToePosition currentPlayer = new TicTacToePosition();
            TicTacToePosition playerX = new TicTacToePosition{ Mark = TicTacToeMarks.X};
            TicTacToePosition playerO = new TicTacToePosition{ Mark = TicTacToeMarks.O};

            Console.WriteLine("Which symbol is starting, X or O");
            string Symbol = Console.ReadLine().ToUpper();
            while (Symbol != "X" && Symbol != "O")
            {
                Console.WriteLine("Invalid symbol! Please choose X or O: ");
                Symbol = Console.ReadLine().ToUpper();
            }
            if(Symbol == "X") {
                currentPlayer = playerX;
            } else {
                currentPlayer = playerO;
            }

            while (grid.Winner == "Ongoing") {
                
                grid.PrintGrid();

                Console.WriteLine($"It is player {currentPlayer}'s turn");

                try {
                    grid.PlaceCharacter(currentPlayer);
                } catch( ArgumentException e) {
                    Console.WriteLine(e.Message);

                    continue;
                }

                Console.WriteLine(grid.Winner);

                currentPlayer = currentPlayer == playerX ? playerO : playerX;

            }
        }
    }
}